<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name></name>
    <message id="pending-tasks" numerus="yes">
        <source>Pending&lt;br/&gt;task(s)</source>
        <extracomment>Pending tasks cover label. Code requires exact line break tag &quot;&lt;br/&gt;&quot;.</extracomment>
        <translation>
            <numerusform>Pending&lt;br/&gt;task</numerusform>
        </translation>
    </message>
    <message id="permission-la-data">
        <source>Taskwarior data directory</source>
        <extracomment>Permission for Taskwarrior directory</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="permission-la-data_description">
        <source>Read and write Taskwarrior data</source>
        <extracomment>Permission description for Whisperfish data storage</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-page-title">
        <source>Create new task</source>
        <extracomment>Create new task page header</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-description">
        <source>Description</source>
        <extracomment>New task page: Task description label</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-description-placeholder">
        <source>Put the trash out</source>
        <oldsource>Put the trash out recur:weekly</oldsource>
        <extracomment>New task page: Task description placeholder</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-priority">
        <source>Priority (optional)</source>
        <extracomment>New task page: Task priority label</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-priority-no-priority">
        <source>No priority</source>
        <extracomment>New task page: priority: No priority</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-priority-high">
        <source>High priority</source>
        <extracomment>New task page: priority: High priority</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-priority-medium">
        <source>Medium priority</source>
        <extracomment>New task page: priority: Medium priority</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-priority-low">
        <source>Low priority</source>
        <extracomment>New task page: priority: Low priority</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-date-selector-none">
        <source>None</source>
        <oldsource>No due date</oldsource>
        <extracomment>Date selector: No due/scheduled date</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-date-selector-eow">
        <source>End of week</source>
        <extracomment>Date selector: end of week</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-date-selector-eoww">
        <source>End of work week</source>
        <extracomment>Date selector: end of work week</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-due">
        <source>Due date</source>
        <extracomment>New task page: Due date selector</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-scheduled">
        <source>Scheduled date</source>
        <extracomment>New task page: Schedule date selector</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-dates-section">
        <source>Due and schedule date</source>
        <extracomment>Create task page: dates section</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="overview-pull-sync">
        <source>Synchronize with server</source>
        <extracomment>Task overview page: pull down: sync</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="overview-pull-create-new">
        <source>Create new task</source>
        <extracomment>Task overview page: pull down: create task</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="overview-header">
        <source>Pending tasks</source>
        <extracomment>Task overview page: header</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-date-selector-today">
        <source>Today</source>
        <extracomment>Date selector</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-date-selector-tomorrow">
        <source>Tomorrow</source>
        <extracomment>Date selector</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-project-selector">
        <source>Project</source>
        <oldsource>Project (optional)</oldsource>
        <extracomment>Project selector: &quot;Project&quot;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-project-selector-no-project">
        <source>No project</source>
        <oldsource>None</oldsource>
        <extracomment>Project selector: No project</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-project-selector-enter-project-name">
        <source>Enter project name</source>
        <extracomment>Project selector: Enter project name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-project-selector-project-name">
        <source>Project name</source>
        <extracomment>Project selector page: project name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-date-selector-select">
        <source>Select date</source>
        <extracomment>Date selector Select custom due/scheduled date</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
