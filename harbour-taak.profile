# -*- mode: sh -*-

# Firejail profile for /usr/bin/harbour-taak

# x-sailjail-translation-catalog = harbour-taak
# x-sailjail-translation-key-description = permission-la-data
# x-sailjail-description = Taskwarrior data storage
# x-sailjail-translation-key-long-description = permission-la-data_description
# x-sailjail-long-description = Read tasks

### PERMISSIONS
# x-sailjail-permission = Internet

whitelist ${HOME}/.task
noblacklist ${HOME}/.task

whitelist ${HOME}/.taskrc
noblacklist ${HOME}/.taskrc

private-bin task
