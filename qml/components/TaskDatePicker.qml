import QtQuick 2.0
import Sailfish.Silica 1.0

Column {
    id: taskDatePickerColumn
    property alias label: modeSelector.label

    readonly property string dateText: {
        (modeSelector.currentIndex == 1) ? Qt.formatDate(datePicker.date, "yyyy-MM-dd") : modeSelector.currentItem.encoded
    }

    width: parent.width
    spacing: Theme.paddingLarge

    ComboBox {
        id: modeSelector

        menu: ContextMenu {
            MenuItem {
                property string encoded: ""
                //: Date selector: No due/scheduled date
                //% "None"
                text: qsTrId("taak-date-selector-none")
            }
            // TODO: the custom date string should be reparsed into Y-M-D, such that it's readable for taskwarrior's cli
            MenuItem {
                property string encoded: ""
                //: Date selector Select custom due/scheduled date
                //% "Select date"
                text: qsTrId("taak-date-selector-select")
            }
            MenuItem {
                property string encoded: "today"
                //: Date selector
                //% "Today"
                text: qsTrId("taak-date-selector-today")
            }
            MenuItem {
                property string encoded: "tomorrow"
                //: Date selector
                //% "Tomorrow"
                text: qsTrId("taak-date-selector-tomorrow")
            }
            MenuItem {
                property string encoded: "eow"
                //: Date selector: end of week
                //% "End of week"
                text: qsTrId("taak-date-selector-eow")
            }
            MenuItem {
                property string encoded: "eoww"
                //: Date selector: end of work week
                //% "End of work week"
                text: qsTrId("taak-date-selector-eoww")
            }
            // TODO: These only work in taskwarrior 2.6
            // MenuItem {
            //     property string encoded: "eonw"
            //     //: Date selector: end of next week
            //     //% "End of next week"
            //     text: qsTrId("taak-date-selector-eonw")
            // }
            // MenuItem {
            //     property string encoded: "eonww"
            //     //: Date selector: end of next work week
            //     //% "End of next work week"
            //     text: qsTrId("taak-date-selector-eonww")
            // }
         }
    }

    DatePicker {
        id: datePicker
        visible: modeSelector.currentIndex == 1
        daysVisible: true
        weeksVisible: true
        monthYearVisible: true
    }
}
