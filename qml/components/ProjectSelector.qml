import QtQuick 2.0
import Sailfish.Silica 1.0

Column {
    id: projectPickerColumn

    readonly property string project: {
        (projectSelector.currentIndex == 1) ? projectName.text : projectSelector.currentItem.encoded
    }

    width: parent.width
    spacing: Theme.paddingLarge

    ComboBox {
        id: projectSelector

        //: Project selector: "Project"
        //% "Project"
        label: qsTrId("taak-project-selector")

        menu: ContextMenu {
            MenuItem {
                property string encoded: ""
                //: Project selector: No project
                //% "No project"
                text: qsTrId("taak-project-selector-no-project")
                color: Theme.secondaryHighlightColor
            }
            MenuItem {
                property string encoded: "enter"
                //: Project selector: Enter project name
                //% "Enter project name"
                text: qsTrId("taak-project-selector-enter-project-name")
                color: Theme.secondaryHighlightColor
            }
            // TODO
         }
    }

    TextArea {
        id: projectName
        //: Project selector page: project name
        //% "Project name"
        label: qsTrId("taak-project-selector-project-name")
        visible: projectSelector.currentIndex == 1
    }
}
