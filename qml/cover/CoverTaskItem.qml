import QtQuick 2.0
import Sailfish.Silica 1.0

Row {
    id: root

    spacing: Theme.paddingSmall

    property var task

    Rectangle {
        id: rectangle

        radius: Theme.paddingSmall/3
        width: Theme.paddingSmall
        height: parent.height - Theme.paddingMedium - Theme.paddingSmall/2
        anchors.verticalCenter: parent.verticalCenter

        color: task.color
    }

    Column {
        Label {
            id: descriptionLabel

            font.pixelSize: Theme.fontSizeSmall
            verticalAlignment: Text.AlignTop
            // fontSizeMode: Text.HorizontalFit
            height: dueLabel.visible ? (root.height * 2 / 3) : root.height
            width: root.width

            maximumLineCount: dueLabel.visible ? 2 : 3
            wrapMode: Text.Wrap

            text: task.description
        }

        Label {
            id: dueLabel

            font.pixelSize: Theme.fontSizeSmall
            verticalAlignment: Text.AlignTop
            height: root.height / 3
            width: root.width

            color: task.overdue ? Theme.highlightColor : Theme.secondaryHighlightColor

            maximumLineCount: 1
            wrapMode: Text.Wrap

            visible: task.due !== null

            text: {
                if (task.due !== null ) {
                    return Format.formatDate(task.due, Formatter.DurationElapsed);
                } else {
                    return "";
                }
            }
        }
    }
}
