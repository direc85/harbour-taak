import QtQuick 2.0
import Sailfish.Silica 1.0

import "../components"

Dialog {
    id: createTaskPage

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    onAccepted: {
        var priorities = ["", "H", "M", "L"];
        tasks.add(description.text, priorities[priority.currentIndex], dueDatePicker.dateText, scheduledDatePicker.dateText, projectSelector.project);
    }

    canAccept: description.text.length > 0

    SilicaFlickable {
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: col.height + Theme.paddingLarge

        VerticalScrollDecorator {}

        Column {
            id: col
            spacing: Theme.paddingLarge
            width: parent.width

            DialogHeader {
                //: Create new task page header
                //% "Create new task"
                title: qsTrId("taak-create-task-page-title")
            }

            TextArea {
                id: description
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                //: New task page: Task description label
                //% "Description"
                label: qsTrId("taak-create-task-description")

                //: New task page: Task description placeholder
                //% "Put the trash out"
                placeholderText: qsTrId("taak-create-task-description-placeholder")
            }

            ProjectSelector {
                id: projectSelector
            }

            ComboBox {
                id: priority

                //: New task page: Task priority label
                //% "Priority (optional)"
                label: qsTrId("taak-create-task-priority")

                menu: ContextMenu {
                    MenuItem {
                        //: New task page: priority: No priority
                        //% "No priority"
                        text: qsTrId("taak-create-task-priority-no-priority")
                    }
                    MenuItem {
                        //: New task page: priority: High priority
                        //% "High priority"
                        text: qsTrId("taak-create-task-priority-high")
                        color: "red"
                    }
                    MenuItem {
                        //: New task page: priority: Medium priority
                        //% "Medium priority"
                        text: qsTrId("taak-create-task-priority-medium")
                        color: "green"
                    }
                    MenuItem {
                        //: New task page: priority: Low priority
                        //% "Low priority"
                        text: qsTrId("taak-create-task-priority-low")
                        color: "blue"
                    }
                 }
            }

            // ------ BEGIN DATES ------
            SectionHeader {
                //: Create task page: dates section
                //% "Due and schedule date"
                text: qsTrId("taak-dates-section")
            }

            TaskDatePicker {
                id: dueDatePicker

                //: New task page: Due date selector
                //% "Due date"
                label: qsTrId("taak-create-task-due")
            }

            TaskDatePicker {
                id: scheduledDatePicker

                //: New task page: Schedule date selector
                //% "Scheduled date"
                label: qsTrId("taak-create-task-scheduled")
            }
            // -------- END DATES --------
        }
    }
}
