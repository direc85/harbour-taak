import QtQuick 2.0
import Sailfish.Silica 1.0

Row {
    id: root

    spacing: Theme.paddingLarge

    property var task

    Rectangle {
        id: rectangle

        radius: Theme.paddingSmall/3
        width: Theme.paddingSmall
        height: parent.height - Theme.paddingMedium - Theme.paddingSmall/2
        anchors.verticalCenter: parent.verticalCenter

        color: task.color
    }

    Column {
        Label {
            id: descriptionLabel

            verticalAlignment: dueLabel.visible? Text.AlignTop : Text.AlignVCenter
            // fontSizeMode: Text.HorizontalFit
            height: dueLabel.visible ? (root.height * 2 / 3) : root.height
            width: root.width

            maximumLineCount: dueLabel.visible ? 1 : 2
            wrapMode: Text.Wrap

            text: task.description
        }

        Label {
            id: dueLabel

            font.pixelSize: Theme.fontSizeSmall
            verticalAlignment: Text.AlignBottom
            height: root.height / 3
            width: root.width

            color: task.overdue ? Theme.highlightColor : Theme.secondaryHighlightColor

            maximumLineCount: 1
            wrapMode: Text.Wrap

            visible: task.due !== null

            text: {
                if (task.due !== null ) {
                    return Format.formatDate(task.due, Formatter.DurationElapsed);
                } else {
                    return "";
                }
            }
        }
    }
}

